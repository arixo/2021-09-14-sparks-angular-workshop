= Enoncé

== Compilateur TypeScript

* Installer le compilateur TypeScript.

[source]
----
npm install -g typescript
----

* Vérifier l'installation de TypeScript

[source]
----
tsc -v
Version 4.3.5
----

== Fichier de configuration `tsconfig.json`

* Exécuter la commande suivante pour générer le fichier de configuration `tsconfig.json` depuis le répertoire `typescript`.

[source]
----
tsc --init
----

* Un fichier tsconfig.json est créé.

* Modifier le paramètre `outDir` comme suit :

[source,json]
----
"outDir": "./js"
----

Les fichiers javascript seront générés dans le répertoire `js`.

== Exemple Meteo

* Créer un fichier `meteo.ts`.

[source]
----
/
    meteo.ts
----

* Se positionnez dans le répertoire `typescript` et lancer le compilateur TypeScript en mode `watch`.

[source]
----
tsc --watch
----

Un fichier `js/meteo.js` est généré. Visualiser le.

[source]
----
/
    /js
        meteo.js
    meteo.ts
----

* Compléter le fichier `meteo.ts` comme suit :

[source,ts]
----
console.log("Meteo App")
----

* Visualiser le fichier `js/meteo.js`.

* Exécuter le code généré :

[source]
----
node js/meteo.js
----

Dans la suite de l'exercice, nous compléterons le fichier `meteo.ts`.

* Créer une classe Meteo.

[source,ts]
----
class Meteo {

}
----

Visualiser le résultat `js/meteo.js`.

[source,js]
----
"use strict";
console.log("Meteo App");
var Meteo = (function () {
    function Meteo() {
    }
    return Meteo;
}());
----

Reconnaissez-vous le `Module Pattern` ?


* Ajouter le constructeur suivant :

[source,ts]
----
class Meteo {

    constructor(private _ville:string, private _temperature:number){
    }

}
----

* Créer un objet Meteo


```ts
// ...

let nantesCeMatin = new Meteo()

```

Vous devriez avoir l'erreur de compilation suivante :

```
meteo.ts(8,21): error TS2554: Expected 2 arguments, but got 0.
```

Savez-vous pourquoi ?

Visualiser le fichier `js/meteo.js` :

[source,js]
----
"use strict";
console.log("Meteo App");
var Meteo = (function () {
    function Meteo(_ville, _temperature) {
        this._ville = _ville;
        this._temperature = _temperature;
    }
    return Meteo;
}());
var nantesCeMatin = new Meteo();
----

Malgré l'erreur de compilation, le fichier généré contient la mise à jour.
Ceci illustre le caractère permissif du compilateur TypeScript.

* Corriger l'erreur de compilation :

[source,ts]
----
let nantesCeMatin = new Meteo('Nantes', 12);
----

* Ajouter la méthode `toString` et afficher son résultat dans le console.

[source,ts]
----
class Meteo {
    constructor(private _ville:string, private _temperature:number){
    }

    // A AJOUTER
    toString():string {
        return  `${this._ville} - ${this._temperature}°C`
    }
}
let nantesCeMatin = new Meteo('Nantes', 12);

// A AJOUTER
console.log(nantesCeMatin.toString())
----

* Visualiser le fichier généré

[source,js]
----
"use strict";
console.log("Meteo App");
var Meteo = (function () {
    function Meteo(`ville, `temperature) {
        this.`ville = `ville;
        this.`temperature = `temperature;
    }
    Meteo.prototype.toString = function () {
        return this.`ville + " - " + this.`temperature + "\u00B0C";
    };
    return Meteo;
}());
var nantesCeMatin = new Meteo('Nantes', 12);
console.log(nantesCeMatin.toString());
----

* Exécuter le code généré :

[source]
----
node js/meteo.js
----

== Exemple Voyage

* Créer un fichier `voyage.ts`.

[source]
----
/typescript
    meteo.ts
    voyage.ts
----

* Créer une classe `Sejour` avec les propriétés **publiques** suivantes :
** nom (string)
** prix (number)

* Créer une classe `SejourService` qui possède une propriété privée de type `Sejour[]`;

* Dans le constructeur de la classe `SejourService`, initialiser la propriété Sejour[] avec plusieurs objets de type `Sejour`.

* Dans la classe `SejourService`, ajouter une méthode de recherche de séjour par nom :
** le service prend en paramètre un nom de type `string`
** le service a un type retour `Sejour | void`.

* Créer une instance de la classe `SejourService`, invoquer la méthode de recherche créée et afficher le résultat dans la console.

