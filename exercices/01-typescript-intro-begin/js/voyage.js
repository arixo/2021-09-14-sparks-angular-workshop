"use strict";
var Sejour = /** @class */ (function () {
    function Sejour() {
        this.nom = 'sejour';
        this.prix = 0;
    }
    return Sejour;
}());
var SejourService = /** @class */ (function () {
    function SejourService() {
        this.typeSejour = [];
        this.typeSejour.push(new Sejour('AllInclusive', 1500));
        this.typeSejour.push(new Sejour('pension complète', 990));
        this.typeSejour.push(new Sejour('demi-pension', 700));
    }
    SejourService.prototype.rechercheSejour = function (p1) {
        for (var i = 0; i < this.typeSejour.length; i++)
            if (this.typeSejour[i].nom == p1)
                return this.typeSejour[i].nom + " : " + this.typeSejour[i].prix + "\u20AC";
    };
    return SejourService;
}());
var monSejour = new SejourService();
