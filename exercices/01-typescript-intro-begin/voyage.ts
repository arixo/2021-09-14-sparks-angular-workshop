
class Sejour {
    public nom: string = 'sejour';
    public prix: number = 0;
}

class SejourService {
    private typeSejour: Sejour[];

    constructor(){
        this.typeSejour = [];
        this.typeSejour.push(new Sejour('AllInclusive', 1500));
        this.typeSejour.push(new Sejour('pension complète', 990));
        this.typeSejour.push(new Sejour('demi-pension', 700));
    }

    rechercheSejour(p1:string):string|void {
        for (var i = 0; i < this.typeSejour.length; i++)
            if (this.typeSejour[i].nom == p1) 
                return `${this.typeSejour[i].nom} : ${this.typeSejour[i].prix}€`;
    }
}

let monSejour = new SejourService();