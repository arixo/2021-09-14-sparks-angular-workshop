const fetch = require('node-fetch');
const {config} = require('./config');

/**
 * Composant de communication avec la Web API /collegues.
 */
class Service {

    /**
     * @return la liste (tableau) des collègues.
     */
    async list() {
        const response = await fetch(config.baseUrlApi);
        const allCols = await response.json();
        return allCols.filter(col => col.nom);
    }

    /**
     * @return le collègue créé avec le nom et le prénom.
     */
    async create(collegue) {
        const response = await fetch(config.baseUrlApi, {
            method: 'post',
            body: JSON.stringify(collegue),
            headers: {'Content-Type': 'application/json'}
        });
        return response.json();
    }

}

exports.Service =  Service;
