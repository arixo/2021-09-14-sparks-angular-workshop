const {Service} = require('./service');

const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

class Presentation {

    constructor(service) {
        this.service = service;
    }

    demarrer() {

        const menu = `
1. Lister les collegues
2. Créer un collègue
99. Sortir

Votre choix svp: `

        rl.question(menu, answer => {

            switch (answer) {
                case "1":
                    console.log(">> Liste des collègues")
                    this.service.list().then(collegues => {
                        const result = collegues
                            .map(c => `${c.id} ${c.nom} ${c.prenom}`)
                            .join('\n');

                        console.log(result);
                    }).catch(err => console.log(err))
                        .finally(() => this.demarrer());
                    break;
                case "2":
                    rl.question('Enter votre nom : ', nom => {
                        rl.question('Enter votre prénom : ', prenom => {
                            this.service.create({nom, prenom})
                                .then(colCree => console.log("Un nouveau collegue crée avec succès, id=", colCree))
                                .finally(() => this.demarrer());

                        });
                    });

                    break;
                case "99":
                    console.log("Au revoir !")
                    rl.close();
                    break;
                default:
                    console.log("un nombre valide svp ;-) ")
                    this.demarrer();
                    break;
            }
        });
    }
}

exports.Presentation = Presentation;
