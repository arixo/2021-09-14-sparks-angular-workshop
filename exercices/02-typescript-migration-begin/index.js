const {Presentation} = require("./presentation");
const {Service} = require("./service");

console.log('** Administration Collegues **');

const service = new Service();
const presentation = new Presentation(service);

presentation.demarrer();
